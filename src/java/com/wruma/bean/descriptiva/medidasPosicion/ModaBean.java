/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.descriptiva.medidasPosicion;

import com.wruma.bean.ConsolaBean;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class ModaBean implements Serializable {

    /**
     * Creates a new instance of medidasBean
     */
    private String variableSeleccionada;
    private String frecuenciaSeleccionada;
    private String nuevoDato;
    private FacesMessage errMsg;

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getFrecuenciaSeleccionada() {
        return frecuenciaSeleccionada;
    }

    public void setFrecuenciaSeleccionada(String frecuenciaSeleccionada) {
        this.frecuenciaSeleccionada = frecuenciaSeleccionada;
    }

    
    
    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }

    public FacesMessage getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(FacesMessage errMsg) {
        this.errMsg = errMsg;
    }

    
    
    public ModaBean() {
        System.out.println(this.hashCode());
    }

    public void prepareModa() {
        
        variableSeleccionada = null;
        frecuenciaSeleccionada =null;
        nuevoDato = null;
    }

    public void ejecutaModa() {
        String cmd;
       if(variableSeleccionada.isEmpty()){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR", "Debe seleccionar datos para la moda"));
                System.out.println("after display");
       } else {
           
           if(frecuenciaSeleccionada.isEmpty()){
              
                if (nuevoDato.isEmpty()) {
                cmd = "moda(" + variableSeleccionada + ")";
            } else {
                cmd = nuevoDato + "<-moda(" + variableSeleccionada + ")";
            }

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("moda " + cmd);

            }else{

                if (nuevoDato.isEmpty()) {
                cmd = "modaFrecs(" + variableSeleccionada + ","+frecuenciaSeleccionada+")";
            } else {
                cmd = nuevoDato + "<-modaFrecs(" + variableSeleccionada + ","+frecuenciaSeleccionada+")";
            }

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("moda " + cmd);
           

            }
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("ModaDlg.hide();");
        
       }
       prepareModa();
    }
    
   

}
