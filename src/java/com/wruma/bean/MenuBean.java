/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Driss
 */
@ManagedBean
@ViewScoped
public class MenuBean implements Serializable {

    /**
     * Creates a new instance of MenuBean
     */
     private static final long serialVersionUID = 4352236420460919694L;
    private UploadedFile file;
 
    public UploadedFile getFile() {
        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }
     
    
    public String ejecutarMenu(String op) {
       if(file!=null){
            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
            System.out.println(file.getFileName());
            String tempDir =(FacesContext.getCurrentInstance().getExternalContext()).getRealPath("/temp/");
            File bfile =new File(tempDir+"/"+file.getFileName());
            uploadedToFile(file, bfile);
            String comando = null;
            if(op.equalsIgnoreCase("script")){
               
              comando ="source(\""+ bfile.getPath().replace("\\", "/") +"\")";
            }else if (op.equalsIgnoreCase("rData")){
                comando ="load(\""+  bfile.getPath().replace("\\", "/") +"\")";
            }
           System.out.println("tempDir = "+tempDir);
           System.out.println("bfile PATH = "+bfile.getPath());
         
           
            System.out.println(comando);  
            consola.setComando(comando);
            consola.ejecutarComando();
            bfile.delete();
            
            
        } else{
            System.out.println("The file object is null.");
        }
       return "";
    }   

    private void uploadedToFile(UploadedFile file, File bfile) {
     
            try{
                InputStream inStream = file.getInputstream();
                
                OutputStream  outStream = new FileOutputStream(bfile);
                byte[] buffer = new byte[1024];

                int length;
                //copy the file content in bytes 
                while ((length = inStream.read(buffer)) > 0){

                    outStream.write(buffer, 0, length);

                }

                inStream.close();
                outStream.close();

                System.out.println("File is copied successful!   "+bfile.getAbsolutePath());
 
    	}catch(IOException e){
    		e.printStackTrace();
    	}   
    }
//    
//   private String convertPath(String tempDir){
//   String toRString = "";
//   
//    StringTokenizer st = new StringTokenizer(tempDir.toLowerCase(), "\\");
//                while (st.hasMoreTokens()) {
//                    toRString += st.nextToken()+"/";
//                }   
//   
//   return toRString;
//   }
}
