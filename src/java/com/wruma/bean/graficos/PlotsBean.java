/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.graficos;

import com.wruma.bean.ConsolaBean;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIGraphic;
import javax.faces.component.UIOutput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class PlotsBean implements Serializable {

    /**
     * Creates a new instance of CentralizacionBean
     */
    
    private ExternalContext contexto;
    private ConsolaBean consola;
    private String operacion;  
     
    private String variable1;
    private String variable2;
    private String variable3;
    private String titulo, subtitulo, xlab, ylab, col; 
    private boolean visorActivo;
    private String urlGtafico;
    private StreamedContent chart;
    private String urlimg;
    
    private Map<String,String> operacionesUnidimencionales;
    private Map<String,String> operacionesTridimencionales;
    private Map<String,String> operacionesBidimencionales;
    private String comando;

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    
    
    public Map<String,String> getOperacionesBidimencionales() {
        return operacionesBidimencionales;
    }

    public void setOperacionesBidimencionales(Map<String,String> operacionesBidimencionales) {
        this.operacionesBidimencionales = operacionesBidimencionales;
    }

    
    
    public Map<String, String> getOperacionesTridimencionales() {
        return operacionesTridimencionales;
    }

    public void setOperacionesTridimencionales(Map<String, String> operacionesTridimencionales) {
        this.operacionesTridimencionales = operacionesTridimencionales;
    }
    
    
    public String getExpresion() {
        return expresion;
    }

    public void setExpresion(String expresion) {
        this.expresion = expresion;
    }
    private String expresion;
    
    

    
    public String getUrlimg() {
        return urlimg;
    }

    public void setUrlimg(String urlimg) {
        this.urlimg = urlimg;
    }
    
   private UIOutput graf;

    public UIOutput getGraf() {
        return graf;
    }

    public void setGraf(UIOutput graf) {
        this.graf = graf;
    }
   
    
    public StreamedContent getChart() {
        
        return chart;
    }

   
    
    public String getUrlGtafico() {
        return urlGtafico;
    }

    public void setUrlGtafico(String urlGtafico) {
        this.urlGtafico = urlGtafico;
    }
   
   public String getVariable1() {
        return variable1;
    }

    public void setVariable1(String variable1) {
        this.variable1 = variable1;
    }
     public String getVariable2() {
        return variable2;
    }

    public void setVariable2(String variable2) {
        this.variable2 = variable2;
    }
     public String getVariable3() {
        return variable3;
    }

    public void setVariable3(String variable3) {
        this.variable3 = variable3;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }

    public String getXlab() {
        return xlab;
    }

    public void setXlab(String xlab) {
        this.xlab = xlab;
    }

    public String getYlab() {
        return ylab;
    }

    public void setYlab(String ylab) {
        this.ylab = ylab;
    }

    public boolean isVisorActivo() {
        return visorActivo;
    }

    public void setVisorActivo(boolean visorActivo) {
        this.visorActivo = visorActivo;
    }
    
    
    List<String> variables = new ArrayList<>();

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }
    
    
    public void ejecutar() throws InterruptedException, FileNotFoundException{
        contexto = FacesContext.getCurrentInstance().getExternalContext();
        consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        
    
      
           //consola.getOut().println(comando);
            
            UUID idOne = UUID.randomUUID();
            consola.getOut().println("jpeg(filename=\"plot"+idOne+".jpg\");"+comando+";dev.off()");
            Thread.sleep(600);
            consola.getSalida().append(comando+"\n> ");
            consola.getHistorialComandos().add(comando);
            setUrlimg("temp/plot"+idOne+".jpg");
            consola.getGraficosSession().add(getUrlimg());
            consola.setGraficoActual(getUrlimg());
            establecerDialogo();
            visorActivo = true;  
       
   }
    
     public static byte [] ImageToByte(File file) throws FileNotFoundException, IOException{
        FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);      
                System.out.println("read " + readNum + " bytes,");
            }
        } catch (IOException ex) {
        }
        byte[] bytes = bos.toByteArray();
        fis.close();
      
     return bytes; 
    }
                        
    
    public PlotsBean() { 
      
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Map<String,String> getOperacionesUnidimencionales() {
        return operacionesUnidimencionales;
    }

    public void setOperacionesUnidimencionales(Map<String,String> operacionesUnidimencionales) {
        this.operacionesUnidimencionales = operacionesUnidimencionales;
    }
    
    @PostConstruct
    public void establecerDialogo(){
        visorActivo=false;
        variable1 = "";
        variable2 = "";
        variable3 = "";
        titulo= "";
        subtitulo= "";
        xlab= "";
        ylab = "";
        operacionesUnidimencionales = new HashMap<>();
        operacionesUnidimencionales.put("Gráfica BarPlot", "barplot");
        operacionesUnidimencionales.put("Gráfica de cajas", "boxplot");
        operacionesUnidimencionales.put("Histograma", "hist");
        operacionesUnidimencionales.put("Gráfica de areas", "pie");
        
        operacionesBidimencionales = new HashMap<>();
        operacionesBidimencionales.put("Gráfica Plot", "plot");
        operacionesBidimencionales.put("Gráfica SunflowerPlot", "sunflowerplot");
        operacionesBidimencionales.put("Gráfica jittering", "jitter");
        operacionesBidimencionales.put("Gráficas condicionales", "bidimencional3");
        operacionesBidimencionales.put("Gráfica Identificación interactiva de puntos", "bidimencional1");
        operacionesBidimencionales.put("Gráficas multivariantes", "pairs");
        operacionesBidimencionales.put("Gráfica de cohen", "assocplot");
        
        operacionesTridimencionales = new HashMap<>();
        operacionesTridimencionales.put(" Tri", "plot");
        operacionesTridimencionales.put(" de cajas", "boxplot");
        operacionesTridimencionales.put("Hist", "hist");
        operacionesTridimencionales.put("Tri  5","");
     }

    public void ejecutarUnidemencional(){
        
        // String cmd = "plot(";
        comando = operacion+"("+variable1;
        if(!titulo.isEmpty()){
           comando+=", main =\""+titulo+"\"";
       }
        if(!subtitulo.isEmpty()){
           comando+=", sub =\""+subtitulo+"\"";
       }
         if(!xlab.isEmpty()){
           comando+=", xlab =\""+xlab+"\"";
       }
          if(!ylab.isEmpty()){
           comando+=", ylab =\""+ylab+"\"";
       }
          if(!col.isEmpty()){
           comando+=", col ="+col;
        }
       comando+=")";
        
        try {
            ejecutar();
        } catch (InterruptedException ex) {
            Logger.getLogger(PlotsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PlotsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void ejecutarBidemencional(){
        if(operacion.equals("jitter")){
         comando = "plot("+operacion+"("+variable1+"),"+operacion+"("+variable2+")";
        } else {
        
         comando = operacion+"("+variable1+","+variable2;
        }
       
        if(!titulo.isEmpty()){
           comando+=", main =\""+titulo+"\"";
       }
        if(!subtitulo.isEmpty()){
           comando+=", sub =\""+subtitulo+"\"";
       }
         if(!xlab.isEmpty()){
           comando+=", xlab =\""+xlab+"\"";
       }
          if(!ylab.isEmpty()){
           comando+=", ylab =\""+ylab+"\"";
       }
       comando+=")";
       
        try {
            ejecutar();
        } catch (InterruptedException ex) {
            Logger.getLogger(PlotsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PlotsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    public void ejecutarTridemencional(){
        
          
        comando = operacion+"("+variable1+","+variable2+","+variable3;
        if(!titulo.isEmpty()){
           comando+=", main =\""+titulo+"\"";
       }
        if(!subtitulo.isEmpty()){
           comando+=", sub =\""+subtitulo+"\"";
       }
         if(!xlab.isEmpty()){
           comando+=", xlab =\""+xlab+"\"";
       }
          if(!ylab.isEmpty()){
           comando+=", ylab =\""+ylab+"\"";
       }
       comando+=")";
        try {
            ejecutar();
        } catch (InterruptedException ex) {
            Logger.getLogger(PlotsBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PlotsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName() );
    
     public void guardarGrafico()throws Exception {
         String tempDir =(FacesContext.getCurrentInstance().getExternalContext()).getRealPath("/");
         String path = urlimg.replace("/","\\");
       
     LOGGER.info("guardar archivo:    "+ tempDir+path);
     
   // file = new DefaultStreamedContent(new FileInputStream(new File(tempDir+path)),"image/jpg", "downloaded_optimus.jpg");
                
     
     
    File fi =new File(tempDir+path);
    InputStream input = new FileInputStream(fi);
    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
    setFile (new DefaultStreamedContent(input, externalContext.getMimeType(fi.getName()), fi.getName()));
    System.out.println("PREP = " + file.getName());

     }
     public void eliminarGrafico(){
       // LOGGER.info("Logging an INFO-level message: guardarGrafico  "+graf.getValue().toString());
        String tempDir =(FacesContext.getCurrentInstance().getExternalContext()).getRealPath("/");
        //StringTokenizer st = new StringTokenizer(urlimg, "/");
        String path = urlimg.replace("/","\\");
        eliminararchivo(tempDir+path);
        Map<String, String> param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    }
    
     private StreamedContent file;
     
      
        
 
    public StreamedContent getFile() {
        return file;
    } 

    public void setFile(StreamedContent file) {
        this.file = file;
    }
     
public void eliminararchivo(String archivo){

    File fichero = new File(archivo);
    if(fichero.delete()){
        contexto = FacesContext.getCurrentInstance().getExternalContext();
        consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        if(consola.getGraficosSession().contains(urlimg)){
            consola.getGraficosSession().remove(urlimg);
            System.out.println("archivo eliminado");
        }
    }
}
  

 
    /*
    private String getCentroCmd(List<String> varbles) {
        String result = "";
        
        int i = 0;
        while(i<varbles.size()-1){
            result+=varbles.get(i)+",";
            i++;
        }
        result+=varbles.get(i++);   
        return result;
    }*/


   
}
