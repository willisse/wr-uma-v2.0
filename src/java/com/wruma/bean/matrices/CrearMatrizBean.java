/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.matrices;

import com.wruma.bean.ConsolaBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class CrearMatrizBean {

    /**
     * Creates a new instance of CrearMatrizBean
     */
    
    private String nombre;
    private String datos;
    private boolean porFila;
    private int nrow=1, ncol=1;

    public int getNcol() {
        return ncol;
    }

    public void setNcol(int ncol) {
        this.ncol = ncol;
    }
  
    

    
    
    public int getNrow() {
        return nrow;
    }

    public void setNrow(int nrow) {
        this.nrow = nrow;
    }
    
    
    

    public boolean isPorFila() {
        return porFila;
    }

    public void setPorFila(boolean porFila) {
        this.porFila = porFila;
    }
    
    

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }
    
    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
   
    public void crear(){
    
        String cmd = "";
        if(nombre.isEmpty()){
            
            cmd = "matrix(c("+datos+"),nrow ="+nrow+",ncol ="+ncol+",byrow = ";
        }else{
            cmd = nombre+"<- matrix(c("+datos+"),nrow ="+nrow+",ncol ="+ncol+",byrow = ";
        }
        if(porFila){
            cmd += "TRUE)";
        }else{
            cmd += "FALSE)";
        }
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
       // dd<- matrix(c(1,2,5,6,1,2,5,6,5,8),2,byrow = TRUE)
        consola.setComando(cmd);
        consola.ejecutarComando();
        reset();
        
    
    }
    
    public CrearMatrizBean() {
    }
    
    
    public void reset(){
     nombre = null;
     datos =null;
     nrow = 1;
     ncol = 1;
     porFila = false;
    }
    
}
