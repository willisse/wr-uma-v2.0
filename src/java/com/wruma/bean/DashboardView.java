/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class DashboardView implements Serializable{

    private DashboardModel model;
     
    @PostConstruct
    public void init() {
        
        DashboardColumn column1 = new DefaultDashboardColumn();
        model = new DefaultDashboardModel();
        
        column1.addWidget("tbMenu");
        column1.addWidget("pgraficos");
        model.addColumn(column1);

    }
    
  
    public DashboardModel getModel() {
        return model;
    }
    
}
