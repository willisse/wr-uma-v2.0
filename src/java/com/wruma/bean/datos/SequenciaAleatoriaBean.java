/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.datos;

import com.wruma.bean.ConsolaBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class SequenciaAleatoriaBean {

    /**
     * Creates a new instance of OrdenarDatosBean
     */
    private String nuevoDato;
    private String varSeleccionada;
    private String datos;
    private boolean ordenar = false;
    private boolean reemplazo = true;
    private Integer longitud;
    private String orden = "FALSE";
    private ExternalContext contexto;
    private ConsolaBean consola;

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }
    
    public boolean isReemplazo() {
        return reemplazo;
    }

    public void setReemplazo(boolean reemplazo) {
        this.reemplazo = reemplazo;
    }
    
    

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }
    
    

    public Integer getLongitud() {
        return longitud;
    }

    public void setLongitud(Integer longitud) {
        this.longitud = longitud;
    }
    
    

    public boolean isOrdenar() {
        return ordenar;
    }

    public void setOrdenar(boolean ordenar) {
        this.ordenar = ordenar;
    }
    
    
    

   
    
    
    public String getVarSeleccionada() {
        return varSeleccionada;
    }

    public void setVarSeleccionada(String varSeleccionada) {
        this.varSeleccionada = varSeleccionada;
    }
    
    

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }
    
    public void ejecutaSeqAleatoria(){
        contexto = FacesContext.getCurrentInstance().getExternalContext();
        consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
         String cmd=""; 
         
        
         if(ordenar){
             if(nuevoDato.isEmpty()){
                 cmd = "sort(sample("+datos+","+longitud;
             } else {
                cmd =nuevoDato+"<-sort(sample("+datos+","+longitud;
             }
             if(reemplazo){
                 cmd+=",replace = TRUE";
             }
             if(orden.equalsIgnoreCase("true")){
                 cmd+="),decreasing = TRUE)";
             } else{
                cmd+="))";
             }
         }else{
             if(nuevoDato.isEmpty()){
                 cmd = "sample("+datos+","+longitud;
             } else {
                cmd =nuevoDato+"<-sample("+datos+","+longitud;
             }
             if(reemplazo){
                 cmd+=",replace = TRUE";
             }
             cmd+=")";
         }
         
        
         
            consola.setComando(cmd);
            consola.ejecutarComando();
    
    
      
      System.out.println("sequencia "+datos+" "+nuevoDato+" "+ordenar+" "+reemplazo+" "+longitud+" "+orden);
      reset();
    }
    
    
    public SequenciaAleatoriaBean() {
    }
    
    public void reset(){
        orden = "FALSE";
        datos="";
        nuevoDato="";
        ordenar = false;
        reemplazo = true;
        longitud=0;
        
        System.out.println("sequencia aleatoria dialog reset");
    }
    
}
