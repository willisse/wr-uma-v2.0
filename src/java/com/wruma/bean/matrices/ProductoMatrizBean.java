/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.matrices;

import com.wruma.bean.ConsolaBean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class ProductoMatrizBean {

    /**
     * Creates a new instance of CrearMatrizBean
     */
    
    private String nombre;
    private String datos;
    private boolean expresion;
   
    private String matA, matB;

    public String getMatA() {
        return matA;
    }

    public void setMatA(String matA) {
        this.matA = matA;
    }

    public String getMatB() {
        return matB;
    }

    public void setMatB(String matB) {
        this.matB = matB;
    }
    

    
    
    public String getFilas() {
        return filas;
    }

    public void setFilas(String filas) {
        this.filas = filas;
    }
    private String filas;
    
    

    public boolean isExpresion() {
        return expresion;
    }

    public void setExpresion(boolean expresion) {
        this.expresion = expresion;
    }
    
    

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }
    
    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
   
    public void crear(){
    
        String cmd = "";
        if(nombre.isEmpty()){
            cmd = "matrix(c("+datos+"),"+filas+",byrow = ";
        }else{
            cmd = nombre+"<- matrix(c("+datos+"),"+filas+",byrow = ";
        }
        if(expresion){
            cmd += "TRUE)";
        }else{
            cmd += "FALSE)";
        }
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
       // dd<- matrix(c(1,2,5,6,1,2,5,6,5,8),2,byrow = TRUE)
        consola.setComando(cmd);
        consola.ejecutarComando();
        reset();
        
    
    }
    
    public ProductoMatrizBean() {
    }
    
     public void producto(){
    
        String cmd = "";
        if(nombre.isEmpty()){
            cmd = matA+"%*%"+matB;
        }else{
            cmd = nombre+"<-"+matA+"%*%"+matB;
        }
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
       // dada<-(dd%*%matrix(c(1,2,5,6,1,2,5,6,5,8),5,byrow = TRUE))
         consola.setComando(cmd);
        
        consola.ejecutarComando();
        reset();
        
    }
    
    public void reset(){
     nombre = null;
     matA = null;
     matB = null;
     datos =null;
     expresion=false;
     
    }
    
}
