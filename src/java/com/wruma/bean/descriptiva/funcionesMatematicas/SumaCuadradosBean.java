/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.descriptiva.funcionesMatematicas;

import com.wruma.bean.ConsolaBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class SumaCuadradosBean {

    /**
     * Creates a new instance of medidasBean
     */
    private String variableSeleccionada;
    private String nuevoDato;

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }

    public SumaCuadradosBean() {
    }

    public void prepareSumaCuadrados() {
        variableSeleccionada = null;
        nuevoDato = null;
    }

    public void ejecutaSumaCuadrados() {
        String cmd;
        if (nuevoDato.isEmpty()) {
            cmd = "sumaCuadrados(" + variableSeleccionada + ")";
        } else {
            cmd = nuevoDato + "<-sumaCuadrados(" + variableSeleccionada + ")";
        }

        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

        consola.setComando(cmd);
        consola.ejecutarComando();

        System.out.println("SumaCuadrados " + cmd);

    }

}
