/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean;

import com.wruma.utils.Consola;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean(eager=true)
@SessionScoped
public class ConsolaBean implements Serializable {
    
    private ProcessBuilder procesoR;
    private Process p;
    private PrintWriter out;
    private LectorFlujoR errorGobbler;
    private LectorFlujoR outputGobbler;
    private StringBuilder salida;
    private String comando;
    private List<String> historialComandos, variables;
    private String varSeleccionada;
    private String tempDirPath;
    private List<String> graficosSession;
    private String graficoActual;
   

    public String getGraficoActual() {
        return graficoActual;
    }

    public void setGraficoActual(String graficoActual) {
        this.graficoActual = graficoActual;
    }
    
    
   
    
    public String getTempDirPath() {
        return tempDirPath;
    }

    public void setTempDirPath(String tempDirPath) {
        this.tempDirPath = tempDirPath;
    }

    
    public StringBuilder getSalida() {
        return salida;
    }

    public void setSalida(StringBuilder salida) {
        this.salida = salida;
    }

    public String getComando() {
        return comando;
    }

    public void setComando(String comando) {
        this.comando = comando;
    }

    public List<String> getHistorialComandos() {
        return historialComandos;
    }

    public void setHistorialComandos(List<String> historialComandos) {
        this.historialComandos = historialComandos;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }

    public String getVarSeleccionada() {
        return varSeleccionada;
    }

    public void setVarSeleccionada(String varSeleccionada) {
        this.varSeleccionada = varSeleccionada;
    }

    public PrintWriter getOut() {
        return out;
    }

    public void setOut(PrintWriter out) {
        this.out = out;
    }

    public List<String> getGraficosSession() {
        Collections.reverse(graficosSession);
       return graficosSession;
        
    }

    public void setGraficosSession(List<String> graficosSession) {
        this.graficosSession = graficosSession;
    }

    
    public ConsolaBean() {
    }
    
    public void ejecutarComando(){
       try {  // controlar si esta vacio commando
         
            historialComandos.add(comando);
            salida.append(comando).append("\n");
            out.println(comando);
            Thread.sleep(250);
            salida.append(outputGobbler.getSalida());
            salida.append(errorGobbler.getSalida()); 
            
            salida.append("> ");
        } catch (InterruptedException ex) {
            Logger.getLogger(Consola.class.getName()).log(Level.SEVERE, null, ex);
        }
       setComando("");
       actualizarVariables();
       
       
       
   }
    
     public void limpiarPantalla(){
         salida = new StringBuilder("> ");
         
        
    }
    
   @PostConstruct
   private void inicializarConsola(){
        String tempDir;
        salida = new StringBuilder();
        tempDir =(FacesContext.getCurrentInstance().getExternalContext()).getRealPath("/temp");
        tempDirPath=new String();
        StringTokenizer st = new StringTokenizer(tempDir.toLowerCase(), "\\");
        while (st.hasMoreTokens()) {
            tempDirPath += st.nextToken()+"/";
        }
        System.out.println(tempDirPath); 
        graficosSession = new ArrayList<>();
            
            historialComandos = new ArrayList<>();
            variables = new ArrayList<>();
        try {
            
            procesoR = new ProcessBuilder()
                    .command("C:\\R\\bin\\i386\\Rterm.exe", "--vanilla", "--no-restore","--no-save","--ess")
                    .redirectErrorStream(true);
            p = procesoR.start();
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(p.getOutputStream())), true);
            errorGobbler = new LectorFlujoR(p.getErrorStream(), "ERROR");
            
// any output?
            outputGobbler = new LectorFlujoR(p.getInputStream(), "OUTPUT");
            
// start gobblers
            outputGobbler.start();
            errorGobbler.start();
            
            Thread.sleep(500);
            salida.append(outputGobbler.getSalida()).append("> ");
            out.println("setwd(\""+tempDirPath+"\")");
            out.println("library(WebRUma)");
        } catch (IOException ex) {
            Logger.getLogger(ConsolaBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(ConsolaBean.class.getName()).log(Level.SEVERE, null, ex);
        }
       
   }

    private void actualizarVariables() {
        
        try {

            out.println("ls.str()");
            Thread.sleep(500);

            StringTokenizer lineas = new StringTokenizer(outputGobbler.getSalida(), "\n");
            variables = new ArrayList<>();
            while (lineas.hasMoreTokens()) {

                StringTokenizer vars = new StringTokenizer(lineas.nextToken());
                String var = vars.nextToken();
                variables.add(var);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Consola.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
   
   
   
   private static class LectorFlujoR  extends Thread implements Serializable {
    private InputStream is;
    private String type;
    private String salida;
    
    private LectorFlujoR(InputStream is, String type) {
        
        this.is = is;
        this.type = type;
        this.salida = new String();
    }

        public String getSalida() {
          //  FacesContext.getCurrentInstance().getMessageList().clear();
            while(salida.startsWith("> ")){
                salida = salida.substring(2, salida.length());
            }
            String res = salida;
            salida = new String();
            if(res.startsWith("Error")){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", res));
                return salida;
            } else {
            return res;
            }
            
        }

        public void setSalida(String salida) {
            this.salida = salida;
        }

        @Override
        public void run() {
            try {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line = "";

                while ((line = br.readLine()) != null) {
                    salida += line + "\n";
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
    
}
