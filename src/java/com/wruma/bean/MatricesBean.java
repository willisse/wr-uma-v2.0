/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class MatricesBean implements Serializable {

    /**
     * Creates a new instance of VariablesBean
     */
    private String varSeleccionada;
    private String nombre;
    private String datos;
    private boolean porFila, expresion;
    private String matA, matB;

    public boolean isExpresion() {
        return expresion;
    }

    public void setExpresion(boolean expresion) {
       
        this.expresion = expresion;
    }
    
    

    public String getMatA() {
        return matA;
    }

    public void setMatA(String matA) {
        this.matA = matA;
    }

    public String getMatB() {
        return matB;
    }

    public void setMatB(String matB) {
        this.matB = matB;
    }

    
    
    public String getFilas() {
        return filas;
    }

    public void setFilas(String filas) {
        this.filas = filas;
    }
    private String filas;
    
    

    public boolean isPorFila() {
        return porFila;
    }

    public void setPorFila(boolean porFila) {
        this.porFila = porFila;
    }
    
    

    public String getDatos() {
        
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }
    
    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    public String getVarSeleccionada() {
        return varSeleccionada;
    }

    public void setVarSeleccionada(String varSeleccionada) {
        this.varSeleccionada = varSeleccionada;
    }
    
    public void eliminar(){
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        consola.setComando("rm("+varSeleccionada+")");
        consola.ejecutarComando();
        
    }
    
    public void crear(){
    
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
       // dd<- matrix(c(1,2,5,6,1,2,5,6,5,8),2,byrow = TRUE)
        consola.setComando(nombre+"<- matrix(c("+datos+"),"+filas+",byrow = TRUE)");
        consola.ejecutarComando();
    
    }
    
    public void producto(){
    
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
       // dada<-(dd%*%matrix(c(1,2,5,6,1,2,5,6,5,8),5,byrow = TRUE))
         consola.setComando(nombre+"<-"+matA+"%*%"+matB);
        
        consola.ejecutarComando();
    
    }
    
    public MatricesBean() {
    }
    
}
