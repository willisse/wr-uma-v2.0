/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.algebra.matrices;

import com.wruma.bean.ConsolaBean;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class VyVPropiosBean implements Serializable {

    /**
     * Creates a new instance of medidasBean
     */
    private String variableSeleccionada;
    
    private String nuevoDato;
   

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }

    public VyVPropiosBean() {
        System.out.println(this.hashCode());
    }

    public void prepareTraspuesta() {
        
        variableSeleccionada = null;
       
        nuevoDato = null;
    }

    public void ejecutaVyVPropios() {
        String cmd;
       if(variableSeleccionada.isEmpty()){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR", "Debe seleccionar datos para los V. V. Propios"));
                System.out.println("after display");
       } else {
           
         
              
                if (nuevoDato.isEmpty()) {
                cmd = "eigen(" + variableSeleccionada + ")";
            } else {
                cmd = nuevoDato + "<-eigen(" + variableSeleccionada + ")";
            }
                

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("VVPropiosMatDlg " + cmd);
           
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("VVPropiosMatDlg.hide();");
        
       }
      
    }
    
   

}
