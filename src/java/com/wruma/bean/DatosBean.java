/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class DatosBean implements Serializable{

    /**
     * Creates a new instance of DatosBean
     */
    private String dato;
    private String nombre;
    private String orden = "FALSE";

    
    
    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }
    
    
    public DatosBean() {
    }
    
    public void ordenar(){
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
       // driss <- sort(x, decreasing = TRUE)
         consola.setComando(nombre+"<-sort("+dato+",decreasing ="+orden);
        
        consola.ejecutarComando();
        
    System.out.println("datos ordenado "+consola.getVarSeleccionada());
    consola.setVarSeleccionada(null);
    }
    
    public void reset(){
    
        dato = null;
        nombre = null;
        orden = "FALSE";
    }
}
