/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean;

import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;

import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class CentralizacionBean implements Serializable {

    /**
     * Creates a new instance of CentralizacionBean
     */
    
    private ExternalContext contexto;
    private ConsolaBean consola;
    private List<String> operacionesSeleccionadas;
    private String variableSeleccionada;
    private List<String> variables;
    private Map<String,String> operaciones;

    public List<String> getOperacionesSeleccionadas() {
        return operacionesSeleccionadas;
    }

    public void setOperacionesSeleccionadas(List<String> operacionesSeleccionadas) {
        this.operacionesSeleccionadas = operacionesSeleccionadas;
    }

    public List<String> getVariables() {
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }

    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }
    
    
    
    public Map<String, String> getOperaciones() {
        return operaciones;
    }

    public void setOperaciones(Map<String, String> operaciones) {
        this.operaciones = operaciones;
    }

   public void ejecutar() throws InterruptedException{
       contexto = FacesContext.getCurrentInstance().getExternalContext();
       consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
       String cmd = new String();
       for (int i = 0; i < operacionesSeleccionadas.size()-1; i++) {
           cmd += operacionesSeleccionadas.get(i)+"("+variableSeleccionada+");";
       }
       cmd += operacionesSeleccionadas.get(operacionesSeleccionadas.size()-1)+"("+variableSeleccionada+")";
       consola.setComando(cmd);
       consola.ejecutarComando();
       establecerDialogo();
      
   }
                        
    
    
    public CentralizacionBean() { 
    }
    
    @PostConstruct
    public void establecerDialogo(){
    
        operacionesSeleccionadas = new ArrayList<>();
        variableSeleccionada = "";
        operaciones = new HashMap<>();
        operaciones.put("Media", "mean");
        operaciones.put("Mediana", "median");
        operaciones.put("Varianza", "var");
        operaciones.put("desviación estándar", "sd");
        operaciones.put("Summary", "summary");
    }
}
