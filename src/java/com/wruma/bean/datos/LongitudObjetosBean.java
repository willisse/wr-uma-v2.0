/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.datos;

import com.wruma.bean.ConsolaBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */

@ManagedBean
@RequestScoped
public class LongitudObjetosBean {

   // private static final int NUMBER_OF_HOURS_IN_A_DAY = 24;
    /**
     * Creates a new instance of OrdenarDatosBean
     */
  
    private String varSeleccionada;
    private String nuevoDato;
   
    
    private ExternalContext contexto;
    private ConsolaBean consola;

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

   
    
    
    
    
    public String getVarSeleccionada() {
        return varSeleccionada;
    }

    public void setVarSeleccionada(String varSeleccionada) {
        this.varSeleccionada = varSeleccionada;
    }
    
    

   
    public void longitud(){
     System.out.println("Longitud"+varSeleccionada);
         String cmd="";
         if(!varSeleccionada.isEmpty()){
            if(nuevoDato.isEmpty()){
                cmd="length("+varSeleccionada+")";
            } else{
                cmd=nuevoDato+" <- length("+varSeleccionada+")";
            }
            contexto = FacesContext.getCurrentInstance().getExternalContext();
            consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
            consola.setComando(cmd);
            consola.ejecutarComando();
         }
         System.out.println("borrar ");
     // prepareOrdenar();
                 
    }
    
    
    public LongitudObjetosBean() {
        
    }
    
    public void prepareLongitud(){
        
        varSeleccionada = null;
        nuevoDato = "";
    }
    
}
