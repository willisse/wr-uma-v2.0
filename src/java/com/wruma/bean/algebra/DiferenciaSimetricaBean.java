package com.wruma.bean.algebra;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.wruma.bean.ConsolaBean;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class DiferenciaSimetricaBean implements Serializable {

    /**
     * Creates a new instance of medidasBean
     */
    private String dato1;
    private String dato2;
    private String nuevoDato;

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getDato1() {
        return dato1;
    }

    public void setDato1(String dato1) {
        this.dato1 = dato1;
    }

    public String getDato2() {
        return dato2;
    }

    public void setDato2(String dato2) {
        this.dato2 = dato2;
    }

   

    public DiferenciaSimetricaBean() {
    }

    public void prepareDiferenciaSimetrica() {
        dato1 = null;
        dato2 = null;
        nuevoDato = null;
    }

    public void ejecutaDiferenciaSimetrica() {
        String cmd;
         ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
         ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));
         if(!dato1.isEmpty() && !dato2.isEmpty()){
            if (nuevoDato.isEmpty()) {
                cmd = "setdiff(" + dato1 + "," + dato2 + ")";
            } else {
                cmd = nuevoDato + "<-setdiff(" + dato1 + "," + dato2 + ")";
            }
            consola.setComando(cmd);
            consola.ejecutarComando();
            System.out.println("DiferenciaSimetrica " + cmd);
         } else {
            consola.getSalida().append("ERROR: Debe seleccionar dos conjuntos\n> ");
        }
    }

}
