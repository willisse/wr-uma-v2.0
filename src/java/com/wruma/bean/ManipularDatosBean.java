/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class ManipularDatosBean implements Serializable {

    /**
     * Creates a new instance of ManipularDatosBean
     */
    
   private String nombre;
   private String datos;
   private boolean porRango;
   private int minR, maxR, paso;
   private ExternalContext contexto;
   private ConsolaBean consola;

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public boolean isPorRango() {
        return porRango;
    }

    public void setPorRango(boolean porRango) {
        this.porRango = porRango;
    }

    public int getMinR() {
        return minR;
    }

    public void setMinR(int minR) {
        this.minR = minR;
    }

    public int getMaxR() {
        return maxR;
        
    }

    public void setMaxR(int maxR) {
        this.maxR = maxR;
    }

    public int getPaso() {
        return paso;
    }

    public void setPaso(int paso) {
        this.paso = paso;
    }

   
			

   

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
 
   
    public void ejecutar () throws InterruptedException {
    
        contexto = FacesContext.getCurrentInstance().getExternalContext();
        consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        if(isPorRango()){
            ejecutarConjuntoPorRango();
        } else {
            ejecutarConjunto();
        }
        establecerDialogo();
    }
        
    
   

    private void ejecutarConjuntoPorRango() throws InterruptedException {
        String cmd;
        if(getNombre().isEmpty()){
            cmd = "seq("+getMinR()+","+getMaxR()+","+getPaso()+")";
         
        } else {
            cmd = getNombre()+"<-seq("+getMinR()+","+getMaxR()+","+getPaso()+")";
           
        }
         
            consola.setComando(cmd);
            consola.ejecutarComando();
    
    }

    private void ejecutarConjunto() throws InterruptedException {
        
        String cmd;
        if(getNombre().isEmpty()){
            cmd = "c("+getDatos()+")";
            
        } else {
            cmd = getNombre()+"<-c("+getDatos()+")";
           
        }
        consola.setComando(cmd);
        consola.ejecutarComando();
        
    }
    
    public ManipularDatosBean() {
    }
    
    @PostConstruct
    public void establecerDialogo(){
        nombre = "";
        datos = "";
        maxR = 0;
        minR = 0;
        porRango = false;
        paso = 1;
    }
}
