/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.datos;

import com.wruma.bean.ConsolaBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class ExtraerDatosBean {

    /**
     * Creates a new instance of OrdenarDatosBean
     */
    private String nuevoDato;
    private String varSeleccionada;
    private Integer indiceCmp;
    private Integer minRango;
    private Integer maxRango;
    private boolean porRango;
    
    
    
    private ExternalContext contexto;
    private ConsolaBean consola;

    public String getVarSeleccionada() {
        return varSeleccionada;
    }

    public void setVarSeleccionada(String varSeleccionada) {
        this.varSeleccionada = varSeleccionada;
    }

    public Integer getIndiceCmp() {
        return indiceCmp;
    }

    public void setIndiceCmp(Integer indiceCmp) {
        this.indiceCmp = indiceCmp;
    }

    public Integer getMinRango() {
        return minRango;
    }

    public void setMinRango(Integer minRango) {
        this.minRango = minRango;
    }

    public Integer getMaxRango() {
        return maxRango;
    }

    public void setMaxRango(Integer maxRango) {
        this.maxRango = maxRango;
    }

    public boolean isPorRango() {
        return porRango;
    }

    public void setPorRango(boolean porRango) {
        this.porRango = porRango;
    }
    
    

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }
    
    public void extraer(){
     System.out.println("Ordenando"+varSeleccionada);
         contexto = FacesContext.getCurrentInstance().getExternalContext();
         consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
         String cmd=""; 
        if(!varSeleccionada.isEmpty()){ 
            if(nuevoDato.isEmpty()){
              cmd = varSeleccionada+"[";
            } else {
              cmd =nuevoDato+" <- "+varSeleccionada+"[";
            }
            if(porRango){
                if(minRango!=null  && maxRango!=null){
                    cmd+=minRango+":"+maxRango+"]"; 
                    consola.setComando(cmd);
                    consola.ejecutarComando();
                } else {
                    consola.getSalida().append("ERROR: Debe seleccionar un mínimo y máximo\n> ");
                }
            } else {
                if(indiceCmp!=null){
                    cmd+=indiceCmp+"]";
                    consola.setComando(cmd);
                    consola.ejecutarComando();
                } else {
                    consola.getSalida().append("ERROR: Debe seleccionar el indice a extraer\n> ");
                }
            }
        } else {
        consola.getSalida().append("ERROR: Debe seleccionar un objeto\n> ");
        }
         prepareExtraer();
        
    
    
      
      System.out.println("extraer ");
     // prepareOrdenar();
    }
    
    
    public ExtraerDatosBean() {
        
        
        
    }
    @PostConstruct
    public void prepareExtraer(){
        System.out.println("extraer dialog reset");
        nuevoDato = "";
        varSeleccionada = "";
        porRango = false;
        minRango = 1;
        maxRango = 1;
        indiceCmp = 1;
        
    }
    
}
