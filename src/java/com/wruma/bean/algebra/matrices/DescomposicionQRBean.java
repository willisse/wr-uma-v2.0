/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.algebra.matrices;

import com.wruma.bean.ConsolaBean;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class DescomposicionQRBean implements Serializable {

    /**
     * Creates a new instance of medidasBean
     */
    private String variableSeleccionada;
    
    private String nuevoDato;
   

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }

    public DescomposicionQRBean() {
        System.out.println(this.hashCode());
    }

    public void prepareDescomposicionQR() {
        
        variableSeleccionada = null;
       
        nuevoDato = null;
    }

    public void ejecutaDescomposicionQR() {
        String cmd;
       if(variableSeleccionada.isEmpty()){
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "ERROR", "Debe seleccionar datos para la traspuesta"));
                System.out.println("after display");
       } else {
           
         
              
                if (nuevoDato.isEmpty()) {
                cmd = "qr(" + variableSeleccionada + ")";
            } else {
                cmd = nuevoDato + "<-qr(" + variableSeleccionada + ")";
            }
                

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("DescomposicionQR " + cmd);
           
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("DescomposicionQRMatDlg.hide();");
        
       }
      
    }
    
   

}
