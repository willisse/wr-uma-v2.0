/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class VariablesBean implements Serializable {

    /**
     * Creates a new instance of VariablesBean
     */
    private String varSeleccionada;
    private String nombreVariable;
    private String datos;

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }
    
    

    public String getNombreVariable() {
        return nombreVariable;
    }

    public void setNombreVariable(String nombreVariable) {
        this.nombreVariable = nombreVariable;
    }
    
    
    public String getVarSeleccionada() {
        return varSeleccionada;
    }

    public void setVarSeleccionada(String varSeleccionada) {
        this.varSeleccionada = varSeleccionada;
    }
    
    public void eliminar(){
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        consola.setComando("rm("+varSeleccionada+")");
        consola.ejecutarComando();
        varSeleccionada = null;
        
    }
  
    public void evaluarVariable(){
    
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        consola.setComando(varSeleccionada);
        consola.ejecutarComando();
        varSeleccionada = null;
    }
   /*
    public void listarVariables(){
    
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        consola.setComando("ls.str()");
        consola.ejecutarComando();
        varSeleccionada = null;
    }
    */
    
    public void crearVariable(){
    
        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        consola.setComando(nombreVariable+"<-c("+datos+")");
        consola.ejecutarComando();
        
        nombreVariable = null;
        datos = null;
        varSeleccionada = null;
    
    }
    
    public VariablesBean() {
    }
    
}
