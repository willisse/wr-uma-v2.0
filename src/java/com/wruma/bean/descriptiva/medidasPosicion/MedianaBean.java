/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.descriptiva.medidasPosicion;

import com.wruma.bean.ConsolaBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class MedianaBean {

    /**
     * Creates a new instance of medidasBean
     */
    private String variableSeleccionada;
    private String frecuenciaSeleccionada;
    private String nuevoDato;

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getFrecuenciaSeleccionada() {
        return frecuenciaSeleccionada;
    }

    public void setFrecuenciaSeleccionada(String frecuenciaSeleccionada) {
        this.frecuenciaSeleccionada = frecuenciaSeleccionada;
    }

    
    
    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }

    public MedianaBean() {
    }

    public void prepareMediana() {
        variableSeleccionada = null;
        frecuenciaSeleccionada = null;
        nuevoDato = null;
    }

    public void ejecutaMediana() {
        String cmd;
        
         if(frecuenciaSeleccionada.isEmpty()){
            if (nuevoDato.isEmpty()) {
                cmd = "median(" + variableSeleccionada + ")";
            } else {
                cmd = nuevoDato + "<-median(" + variableSeleccionada + ")";
            }

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("mediana " + cmd);
         }else{
          
            if (nuevoDato.isEmpty()) {
                cmd = "medianaFrecs(" + variableSeleccionada + "," + frecuenciaSeleccionada + ")";
            } else {
                cmd = nuevoDato + "<-medianaFrecs(" + variableSeleccionada + "," + frecuenciaSeleccionada + ")";
            }

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("mediana " + cmd);

        }

    }

}
