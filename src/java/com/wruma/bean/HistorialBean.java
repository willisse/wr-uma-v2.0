/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@SessionScoped
public class HistorialBean implements Serializable {

    /**
     * Creates a new instance of HistorialBean
     */
    
    private ConsolaBean consola;
    private List<String> historial; // lista que almacena las variables almacenadas
    private List<String> comandosSeleccionados; // Puede ejecutar mas de un comando.
    private ExternalContext contexto;
    
    public HistorialBean() {
    }

    public List<String> getHistorial() {
        return consola.getHistorialComandos();
    }

    public void setHistorial(List<String> historial) {
        this.historial = historial;
    }

    public List<String> getComandosSeleccionados() {
        return comandosSeleccionados;
    }

    public void setComandosSeleccionados(List<String> comandosSeleccionados) {
        this.comandosSeleccionados = comandosSeleccionados;
    }
    
     public void ejecutarHistorial() throws InterruptedException{
       // consola.ejecutarHistoricos(comandosSeleccionados); 
        if(!comandosSeleccionados.isEmpty()){
         String cmd = new String();
        
            for (int i = 0; i < comandosSeleccionados.size()-1; i++) {
                cmd += comandosSeleccionados.get(i)+";";
            }
        cmd+=comandosSeleccionados.get(comandosSeleccionados.size()-1);
        
         consola.setComando(cmd);
         consola.ejecutarComando();
        }
       comandosSeleccionados=null;
   }
     
     public void editarSeleccionados(){
         String cmd = new String();
        if(!comandosSeleccionados.isEmpty()){
        for (int i = 0; i < comandosSeleccionados.size()-1; i++) {
            cmd += comandosSeleccionados.get(i)+";";
        }
        cmd+=comandosSeleccionados.get(comandosSeleccionados.size()-1);
        }
        consola.setComando(cmd);
       comandosSeleccionados = null;
     }
  public void borrarSeleccionadosHistorial(){
       if(!comandosSeleccionados.isEmpty()){
         List<String> comandos = (ArrayList<String>) getHistorial();
         comandos.removeAll(comandosSeleccionados);
            consola.setHistorialComandos(comandos);
            comandosSeleccionados = null;
          
       }
      
  }
    public void limpiarHistorial(){
       
            consola.setHistorialComandos(new ArrayList<String>());
            historial = consola.getHistorialComandos();
            comandosSeleccionados = null;
        
    }
    
    @PostConstruct
    private void inicializar(){
    
        contexto = FacesContext.getCurrentInstance().getExternalContext();
        consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
        historial = consola.getHistorialComandos();
        
    }
    
}
