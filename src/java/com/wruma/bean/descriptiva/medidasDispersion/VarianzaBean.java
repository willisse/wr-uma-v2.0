/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.descriptiva.medidasDispersion;

import com.wruma.bean.ConsolaBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class VarianzaBean {

    /**
     * Creates a new instance of medidasBean
     */
    private String variableSeleccionada;
    private String nuevoDato;

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }

    public VarianzaBean() {
    }

    public void prepareVarianza() {
        variableSeleccionada = null;
        nuevoDato = null;
    }

    public void ejecutaVarianza() {
        String cmd;
        if (nuevoDato.isEmpty()) {
            cmd = "varianzaPoblacional(" + variableSeleccionada + ")";
        } else {
            cmd = nuevoDato + "<-varianzaPoblacional(" + variableSeleccionada + ")";
        }

        ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
        ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

        consola.setComando(cmd);
        consola.ejecutarComando();

        System.out.println("varianza " + cmd);

    }

}
