/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wruma.bean.descriptiva.medidasPosicion;

import com.wruma.bean.ConsolaBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class MediaBean {

    /**
     * Creates a new instance of medidasBean
     */
    private String variableSeleccionada;
    private String frecuenciaSeleccionada;
    private String nuevoDato;

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }

    public String getFrecuenciaSeleccionada() {
        return frecuenciaSeleccionada;
    }

    public void setFrecuenciaSeleccionada(String frecuenciaSeleccionada) {
        this.frecuenciaSeleccionada = frecuenciaSeleccionada;
    }
    
    

    public String getVariableSeleccionada() {
        return variableSeleccionada;
    }

    public void setVariableSeleccionada(String variableSeleccionada) {
        this.variableSeleccionada = variableSeleccionada;
    }

    public MediaBean() {
    }

    public void prepareMedia() {
        variableSeleccionada = null;
        frecuenciaSeleccionada = null;
        nuevoDato = null;
    }

    public void ejecutaMedia() {
        String cmd;
        
        if(frecuenciaSeleccionada.isEmpty()){
        
            if (nuevoDato.isEmpty()) {
                cmd = "mean(" + variableSeleccionada + ")";
            } else {
                cmd = nuevoDato + "<-mean(" + variableSeleccionada + ")";
            }

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("media " + cmd);
        
        }else{
        
            if (nuevoDato.isEmpty()) {
                cmd = "mediaFrecs(" + variableSeleccionada + ","+frecuenciaSeleccionada+")";
            } else {
                cmd = nuevoDato + "<-mediaFrecs(" + variableSeleccionada + ","+frecuenciaSeleccionada+")";
            }

            ExternalContext contexto = FacesContext.getCurrentInstance().getExternalContext();
            ConsolaBean consola = ((ConsolaBean) contexto.getSessionMap().get("consolaBean"));

            consola.setComando(cmd);
            consola.ejecutarComando();

            System.out.println("media " + cmd);
        
        
        }

    }

}
