/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Driss
 */
public class Consola {
    
    private final static Logger LOGGER = Logger.getLogger(Consola.class.getName());
    private ProcessBuilder procesoR;
    private Process p;
    private StringBuilder salida;
    private PrintWriter out;
    private StreamReader errorGobbler;
    private StreamReader outputGobbler;
    private List<String> historialComandos, variables;

    public ProcessBuilder getProcesoR() {
        return procesoR;
    }

    public Process getP() {
        return p;
    }
    
    
    
    public Consola(){
        try {
            this.salida = new StringBuilder();
            this.historialComandos = new ArrayList<>();
            this.variables = new ArrayList<>();
            procesoR = new ProcessBuilder()
                    .command("C:\\R\\bin\\i386\\Rterm.exe", "--vanilla", "--no-restore","--no-save","--ess")
                    .redirectErrorStream(true);
            p = procesoR.start();
            
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(p.getOutputStream())), true);
            errorGobbler = new StreamReader(p.getErrorStream(), "ERROR");
            
// any output?
            outputGobbler = new StreamReader(p.getInputStream(), "OUTPUT");
            
// start gobblers
            outputGobbler.start();
            errorGobbler.start();
            Thread.sleep(200);
            salida.append(outputGobbler.getSalida()).append("> ");
            
            
        } catch (InterruptedException ex) {
            Logger.getLogger(Consola.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Consola.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }

    public StringBuilder getSalida() {
        return salida;
    }

    public void setSalida(StringBuilder salida) {
        this.salida = salida;
    }

    public List<String> getVariables() {
        String respInterno;
        try {
            respInterno = ejecutarComandoInterno("ls.str()");
            StringTokenizer lineas=new StringTokenizer(respInterno, "\n");
	
            while(lineas.hasMoreTokens()){
            //String var1 = lineas.nextToken(":");
           StringTokenizer vars = new StringTokenizer(lineas.nextToken());
           String var = vars.nextToken();
           if(!variables.contains(var)){
           variables.add(var);
           }
           
        }
        } catch (InterruptedException ex) {
            Logger.getLogger(Consola.class.getName()).log(Level.SEVERE, null, ex);
        }
        return variables;
    }

    public void setVariables(List<String> variables) {
        this.variables = variables;
    }
    
    
    
    public void ejecutar(String comando){
        try {  // controlar si esta vacio commando
            historialComandos.add(comando);
            salida.append(comando).append("\n");
            out.println(comando);
            Thread.sleep(500);
            salida.append(outputGobbler.getSalida());
            salida.append(errorGobbler.getSalida());
            salida.append("> ");
        } catch (InterruptedException ex) {
            Logger.getLogger(Consola.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void destruir() {
        try {
            errorGobbler.destroy();
            outputGobbler.destroy();
            this.p.destroy();
       
            this.finalize();
        } catch (Throwable ex) {
            Logger.getLogger(Consola.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    public void ejecutarHistoricos(List<String> comandosSeleccionados) {
        
    String cmd = new String();
        if(!comandosSeleccionados.isEmpty()){
        for (int i = 0; i < comandosSeleccionados.size()-1; i++) {
            cmd += comandosSeleccionados.get(i)+";";
        }
        cmd+=comandosSeleccionados.get(comandosSeleccionados.size()-1);
        }
        ejecutar(cmd);
    }

    public void limpiarHistorial() {
        historialComandos = new ArrayList<>();
     }

    public List<String> getHistorialComandos() {
        return historialComandos;
    }

    public void limpiarPantalla(){
        setSalida(new StringBuilder());
    }

    private String ejecutarComandoInterno(String lsstr) throws InterruptedException {
   
            out.println(lsstr);
            Thread.sleep(500);
            return outputGobbler.getSalida();
            
       
    }
    
    private static class StreamReader  extends Thread {
    InputStream is;
    String type;
    String salida;

    private StreamReader(InputStream is, String type) {
        this.is = is;
        this.type = type;
        this.salida = new String();
    }

        public String getSalida() {
            while(salida.startsWith("> ")){
                salida = salida.substring(2, salida.length());
            }
            String res = salida;
            salida = new String();
            return res;
        }

        public void setSalida(String salida) {
            this.salida = salida;
        }

    
    
    @Override
    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = "";
            
            while ((line = br.readLine()) != null)
                salida +=line +"\n";
                
                LOGGER.log(Level.WARNING, line+"\n");
                
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    }
    
}
