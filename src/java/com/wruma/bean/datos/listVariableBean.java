/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.datos;

import com.wruma.bean.ConsolaBean;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;


/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class listVariableBean {

    /**
     * Creates a new instance of listVariableBean
     */
    
    
    @ManagedProperty(value = "#{consolaBean}")
    private ConsolaBean consola;

    public ConsolaBean getConsola() {
        return consola;
    }

    public void setConsola(ConsolaBean consola) {
        this.consola = consola;
    }
     
   
    
    public void listarVariables(int buttonIndex){
        String cmd ="";
        if(buttonIndex==0){
            cmd = "ls.str()";
        } else if(buttonIndex == 1){
            cmd = "ls()";
        }
        
        if(consola.getVariables().isEmpty()){
            consola.getHistorialComandos().add(cmd);
            consola.getSalida().append(cmd+": No hay objetos\n> ");
        } else {
            consola.setComando(cmd);
            consola.ejecutarComando();
        }
    }
    public listVariableBean() {
    }
    
   
}
