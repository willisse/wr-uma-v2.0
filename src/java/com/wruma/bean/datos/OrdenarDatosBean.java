/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.datos;

import com.wruma.bean.ConsolaBean;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */
@ManagedBean
@RequestScoped
public class OrdenarDatosBean {

    /**
     * Creates a new instance of OrdenarDatosBean
     */
    private String nuevoDato;
    private String varSeleccionada;
    private String orden = "FALSE";
    
    private ExternalContext contexto;
    private ConsolaBean consola;

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }
    
    
    public String getVarSeleccionada() {
        return varSeleccionada;
    }

    public void setVarSeleccionada(String varSeleccionada) {
        this.varSeleccionada = varSeleccionada;
    }
    
    

    public String getNuevoDato() {
        return nuevoDato;
    }

    public void setNuevoDato(String nuevoDato) {
        this.nuevoDato = nuevoDato;
    }
    
    public void ordenar(){
     System.out.println("Ordenando"+varSeleccionada);
         contexto = FacesContext.getCurrentInstance().getExternalContext();
         consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
         String cmd=""; 
        if(!varSeleccionada.isEmpty()){ 
            if(nuevoDato.isEmpty()){
              cmd = "sort("+varSeleccionada;
            } else {
                cmd =nuevoDato+" <- sort("+varSeleccionada;
            }
            if(!orden.equalsIgnoreCase("FALSE")){
                cmd +=", decreasing = TRUE)";
            }
            cmd +=")";
        consola.setComando(cmd);
        consola.ejecutarComando();
        }
        
        
    
    
      
      System.out.println("ordenar ");
     // prepareOrdenar();
    }
    
    
    public OrdenarDatosBean() {
        
        
        
    }
    
    public void prepareOrdenar(){
        System.out.println("ordenar dialog reset");
        nuevoDato = "";
        varSeleccionada = "";
        orden = "FALSE";
    }
    
}
