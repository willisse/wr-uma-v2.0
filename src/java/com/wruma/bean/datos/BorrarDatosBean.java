/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wruma.bean.datos;

import com.wruma.bean.ConsolaBean;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Driss
 */

@ManagedBean
@RequestScoped
public class BorrarDatosBean {

   // private static final int NUMBER_OF_HOURS_IN_A_DAY = 24;
    /**
     * Creates a new instance of OrdenarDatosBean
     */
  
    private List<String> varsSeleccionadas;
    private String borraTodo = "FALSE";
    
    private ExternalContext contexto;
    private ConsolaBean consola;

    public String getBorraTodo() {
        return borraTodo;
    }

    public void setBorraTodo(String borraTodo) {
        this.borraTodo = borraTodo;
    }
    
    
    public List<String> getVarsSeleccionadas() {
        return varsSeleccionadas;
    }

    public void setVarsSeleccionadas(List<String> varsSeleccionadas) {
        this.varsSeleccionadas = varsSeleccionadas;
    }
    
    

   
    public void borrar(){
     System.out.println("Borrando"+varsSeleccionadas);
     contexto = FacesContext.getCurrentInstance().getExternalContext();
     consola = ((ConsolaBean)contexto.getSessionMap().get("consolaBean"));
         String cmd="";
         if(!consola.getVariables().isEmpty() && borraTodo.equalsIgnoreCase("TRUE")){
            cmd="rm(list=ls(all=TRUE))";
            consola.setComando(cmd);
            consola.ejecutarComando();
         } else if(!consola.getVariables().isEmpty()){
          int i = 0;
                cmd="rm(";
                while (i<varsSeleccionadas.size()-1) {
                    cmd+=varsSeleccionadas.get(i)+",";
                    i++;
                }
                cmd+=varsSeleccionadas.get(i)+")";
                consola.setComando(cmd);
                consola.ejecutarComando();
         }
            
        
            
      System.out.println("borrar ");
     // prepareOrdenar();
    }
    
    
    public BorrarDatosBean() {
        
    }
    
    public void prepareBorrar(){
        
        varsSeleccionadas = null;
        borraTodo = "FALSE";
    }
    
}
